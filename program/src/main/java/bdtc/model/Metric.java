package bdtc.model;

import lombok.*;


/**
 * Класс для объекта метрики
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Metric {
    /** Название метрики */
    private MetricName name;
    /** Время создания метрики, мс */
    private long metricTimestamp;
    /** Значение метрики */
    private int metricValue;
}
