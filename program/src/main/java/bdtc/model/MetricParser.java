package bdtc.model;

public class MetricParser {

    /**
     * Метод позволяет распарсить строку и создать объект класса Metric
     * @param line распаршиваема строка
     * @return Metric - полученный объект класса Metric
     * @throws ParseException возвращается в случае ошибки парсинга или если не найдена метрика в справочнике MetricName
     */
    public static Metric parse(String line) throws ParseException {
        Metric metric = new Metric();
        String[] strings = line.split(", ");
        if (strings.length != 3)
            throw new ParseException();
        try {
            metric.setName(MetricName.retrieveById(Integer.parseInt(strings[0])));
            metric.setMetricTimestamp(Long.parseLong(strings[1]));
            metric.setMetricValue(Integer.parseInt(strings[2]));
        } catch (Exception exception) {
            throw new ParseException();
        }
        if (metric.getName() == MetricName.UNDEFINED)
            throw new ParseException();
        return metric;
    }
}
