import bdtc.model.Metric;
import bdtc.model.MetricName;
import bdtc.model.MetricParser;
import bdtc.model.ParseException;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;


public class MetricParserTest {

    long date = new Date().getTime();
    private final String testMalformedMetric = "mama, mila, ramu";
    private final String testMalformedMetric2Words = "mama ramu";
    private final String testMetricUndefined = "100, 1510670916247, 10";
    private final String testMetric = "1, " + date + ", 10";
    private Metric expectedMetric = new Metric(MetricName.N1, date, 10);

    @Test
    public void testParsePassed() throws ParseException {
        Metric metric = MetricParser.parse(testMetric);
        assertEquals("Expected success", expectedMetric, metric);
    }

    @Test(expected = ParseException.class)
    public void testParseFailed2Words() throws ParseException {
        MetricParser.parse(testMalformedMetric2Words);
    }

    @Test(expected = ParseException.class)
    public void testParseFailed() throws ParseException {
        MetricParser.parse(testMalformedMetric);
    }

    @Test(expected = ParseException.class)
    public void testParseUndefined() throws ParseException {
        MetricParser.parse(testMetricUndefined);
    }
}

