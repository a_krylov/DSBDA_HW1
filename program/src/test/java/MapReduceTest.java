import bdtc.lab1.HW1Mapper;
import bdtc.lab1.HW1Reducer;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class MapReduceTest {

    private MapDriver<LongWritable, Text, Text, IntWritable> mapDriver;
    private ReduceDriver<Text, IntWritable, Text, FloatWritable> reduceDriver;
    private MapReduceDriver<LongWritable, Text, Text, IntWritable, Text, FloatWritable> mapReduceDriver;

    private final String testMetric = "1, 1616188260099, 10";
    private final String testMetric1 = "1, 1616188260105, 20";
    private final String ExpectedMapperResult = "Node1CPU, 1616188260000, 60s";
    private final int timeRangeForMetrics = 60;

    @Before
    public void setUp() {
        HW1Mapper mapper = new HW1Mapper();
        mapper.setTimeRangeForMetrics(timeRangeForMetrics);
        HW1Reducer reducer = new HW1Reducer();
        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    @Test
    public void testMapper() throws IOException {
        mapDriver
                .withInput(new LongWritable(), new Text(testMetric))
                .withOutput(new Text(ExpectedMapperResult), new IntWritable(10))
                .runTest();
    }

    @Test
    public void testReducer() throws IOException {
        List<IntWritable> values = new ArrayList<IntWritable>();
        values.add(new IntWritable(10));
        values.add(new IntWritable(20));
        reduceDriver
                .withInput(new Text(ExpectedMapperResult), values)
                .withOutput(new Text(ExpectedMapperResult), new FloatWritable(15))
                .runTest();
    }

    @Test
    public void testMapReduce() throws IOException {
        mapReduceDriver
                .withInput(new LongWritable(), new Text(testMetric))
                .withInput(new LongWritable(), new Text(testMetric1))
                .withOutput(new Text(new Text(ExpectedMapperResult)), new FloatWritable(15))
                .runTest();
    }
}
