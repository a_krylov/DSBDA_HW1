import java.io.*;
import java.util.Date;

public class Generator {
    /**
     * Генератор создает файлы в директории DSDBA_HW1/input/program.
     * Каждая строка создается с рандомными параметрами:
     * -metricId будет в диапазоне от 1 до 10 (10 при этом нет в MetricName  и поэтому будут ошибки в counter)
     * -timestamp, в мс, будет начинаться с текущего время и дальше увеличиваться на величину от 1 до 1000 сек
     * -value будет в диапазоне от 1 до 1000
     *
     * @param args 2 аргумента на вход
     *             1) количество файлов для генерации, целое число
     *             2) количество строк в каждом файле, целое число
     */
    public static void main(String[] args) {
        //  валидация на входные параметры
        if (args.length != 2) {
            throw new RuntimeException("You should input 2 parametrs");
        }
        String path = "../input/program";
        int numberFiles = Integer.parseInt(args[0]);
        int numberLines = Integer.parseInt(args[1]);
//        int numberFiles = 3;
//        int numberLines = 300;

        for (int i = 1; i < numberFiles + 1; i++) {
            PrintStream fileOut;
            try {
                fileOut = new PrintStream(path + "/input" + i + ".txt");
                for (int j = 0; j < numberLines; j++) {
                    String metricId = String.valueOf(randomInt(1, 10));
                    String metricTimeStamp = String.valueOf(new Date().getTime() + randomInt(1, 1000000));
                    String metricValue = String.valueOf(randomInt(1, 1000));
                    String metric = String.join(", ", metricId, metricTimeStamp, metricValue);
                    fileOut.println(metric);
                }
                fileOut.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private static int randomInt(int min, int max) {
        return (int) (Math.random() * (max - min + 1) + min);
    }

}
